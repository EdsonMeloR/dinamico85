<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Formulário Admin</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="box-cadastro">
        <div id="formulario-menor">
            <form id="frmadministrador" name="frmadministrador" action="op_administrador.php" method="POST">
                    <legend>Cadastro de administrador</legend>
                    <fieldset>
                        <input type="hidden" id="id" name="id">
                        <label>
                            Nome<br>
                            <input type="text" id="txt_nome" name="txt_nome" required>
                        </label>
                        <label>
                            Email<br>
                            <input type="text" id="txt_email" name="txt_email" required>
                        </label>
                        <label>
                            Login<br>
                            <input type="text" id="txt_login" name="txt_login" required>
                        </label>
                        <label>
                            Senha<br>
                            <input type="password" id="txt_senha" name="txt_senha" required>
                        </label>
                        <label>
                            Confirmar senha<br>
                            <input type="password" id="txt_senha_confirmar" name="txt_senha_confirmar" required>
                        </label>
                        <label>
                            <br>
                            <input type="submit" value="Cadastrar" name="btn_cadastrar" >
                        </label>
                        <span>
                            <?php
                                if(isset($_GET['msg']))
                                {
                                    if($_GET['msg']=='ok')
                                    {
                                        echo 'Sucesso';
                                    }
                                    else if ($_GET['msg']=='errokey')
                                    {
                                        echo 'As senhas devem ser iguais';
                                    }
                                    else
                                    {
                                        echo 'Erro';
                                    }
                                }
                            ?>
                        </span>
                    </fieldset>
            </form>
        </div>
    </div>
</body>
</html>